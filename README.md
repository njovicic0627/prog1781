Copy the code into Visual Studio and build a solution. Then run the program by exeucity the .exe file found in bin/Debug folder of the built solution.

I added an MIT licence to my project since "it basically allows developers to modify or re-calibrate source code according to their preferences. The MIT license always includes a copyright statement and a disclaimer." (Kiuwan, 2019).  
https://www.kiuwan.com/blog/a-comparison-of-the-most-popular-open-source-licenses/